import 'package:flutter/material.dart';
import 'package:module3/Screens/ShirtsPage/ShirtsPage.dart';
import 'package:module3/Screens/ShoesPage/ShoesPage.dart';
import 'package:module3/Screens/BottomsPage/BottomsPage.dart';
// import 'package:module3/Screens/TopsPage/TopsPage.dart';
import 'package:module3/Widgets/SearchBar.dart';
import 'package:module3/Widgets/createProductDialog.dart';
import 'package:module3/Widgets/default_button.dart';

class HomeBody extends StatefulWidget {
  const HomeBody({Key? key}) : super(key: key);

  @override
  State<HomeBody> createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> with TickerProviderStateMixin {
  late TabController tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(
      initialIndex: 0,
      length: 3,
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child:
          // SingleChildScrollView(
          //   child:
          Column(
        children: [
          // SearchBar(),
          // SizedBox(
          //   height: 10,
          //   child: Container(
          //     color: Color(0xFFf5f6f7),
          //   ),
          // ),
          DefaultButton(
            isLoading: false,
            press: () async {
              return await showDialog(
                  context: context,
                  builder: (context) {
                    bool isChecked = false;
                    return CreateProductDialog();
                  });
            },
            text: 'Add Product',
          ),
          SizedBox(
            height: 10,
            child: Container(
              color: Color(0xFFf5f6f7),
            ),
          ),
          PreferredSize(
            preferredSize: Size.fromHeight(50.0),
            child: TabBar(
              indicatorColor: Theme.of(context).primaryColor.withOpacity(0.7),
              controller: tabController,
              labelColor: Theme.of(context).primaryColor,
              tabs: [
                Tab(
                  text: 'Shoes',
                ),
                Tab(
                  text: 'Shirts',
                ),
                Tab(
                  text: 'Bottoms',
                )
              ], // list of tabs
            ),
          ),
          SizedBox(
            height: 10,
            child: Container(
              color: Color(0xFFf5f6f7),
            ),
          ),
          Expanded(
              child: TabBarView(
            controller: tabController,
            children: [
              ShoesPage(),
              // child: CategoryPage(slug: 'categories/'),

              ShirtsPage(),
              // child: BrandHomePage(slug: 'brands/?limit=20&page=1'),

              BottomsPage(),
              // child: ShopHomePage(
              //   slug: 'custom/shops/?page=1&limit=15',
              // ),
              //0 class name
            ],
          )),
        ],
      ),
      // ),
    );
  }
}
