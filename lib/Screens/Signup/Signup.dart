import 'package:flutter/material.dart';
import 'package:module3/Screens/Signup/SignupBody.dart';

class Signup extends StatelessWidget {
  const Signup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.black,
              )),
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text(
            'Sign up ',
            style: TextStyle(color: Colors.black),
          ),
        ),
        body: SignupBody());
  }
}
