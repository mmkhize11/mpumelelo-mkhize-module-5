import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:module3/Common/Common.dart';

import 'package:module3/Screens/Dashboard/Home.dart';
import 'package:module3/Widgets/default_button.dart';
import 'package:module3/Widgets/form_error.dart';

class SignupBody extends StatefulWidget {
  const SignupBody({Key? key}) : super(key: key);

  @override
  State<SignupBody> createState() => _SignupBodyState();
}

class _SignupBodyState extends State<SignupBody> {
  // final _formKey = GlobalKey<FormState>();
  final _formKey = GlobalKey<FormBuilderState>();
  late String email;
  late String password;
  late String conform_password;
  bool remember = false;
  final List<String> errors = [];
  // late File _image;
  // String imageURL = "";
  // late File ImageFile;

  bool _isObscureFirst = true;

  bool _isObscureSecond = true;
  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error!);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FormBuilder(
            key: _formKey,
            child: Column(children: [
              SizedBox(height: 30),
              FormBuilderTextField(
                name: "Email",
                keyboardType: TextInputType.emailAddress,
                onSaved: (newValue) => email = newValue!,
                onChanged: (value) {
                  if (value!.isNotEmpty) {
                    removeError(error: kEmailNullError);
                  } else if (emailValidatorRegExp.hasMatch(value)) {
                    removeError(error: kInvalidEmailError);
                  }
                  return null;
                },
                validator: (value) {
                  if (value!.isEmpty) {
                    addError(error: kEmailNullError);
                    return "";
                  } else if (!emailValidatorRegExp.hasMatch(value)) {
                    addError(error: kInvalidEmailError);
                    return "";
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: "Email",
                  hintText: "Enter your email",
                  // If  you are using latest version of flutter then lable text and hint text shown like this
                  // if you r using flutter less then 1.20.* then maybe this is not working properly
                  floatingLabelBehavior: FloatingLabelBehavior.auto,
                  suffixIcon:
                      Icon(Icons.email, color: Theme.of(context).primaryColor),
                ),
              ),
              SizedBox(height: 30),
              // buildPasswordFormField(),
              FormBuilderTextField(
                name: "Password",
                obscureText: _isObscureFirst,
                // obscureText: true,
                onSaved: (newValue) => password = newValue!,
                onChanged: (value) {
                  if (value!.isNotEmpty) {
                    removeError(error: kPassNullError);
                  } else if (value.length >= 8) {
                    removeError(error: kShortPassError);
                  }
                  password = value;
                },
                validator: (value) {
                  if (value!.isEmpty) {
                    addError(error: kPassNullError);
                    return "";
                  } else if (value.length < 8) {
                    addError(error: kShortPassError);
                    return "";
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: "Password",
                  hintText: "Enter your password",
                  // If  you are using latest version of flutter then lable text and hint text shown like this
                  // if you r using flutter less then 1.20.* then maybe this is not working properly
                  floatingLabelBehavior: FloatingLabelBehavior.auto,
                  suffixIcon: IconButton(
                      icon: Icon(
                          _isObscureFirst
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: _isObscureFirst
                              ? Colors.grey
                              : Theme.of(context).primaryColor),
                      onPressed: () {
                        setState(() {
                          _isObscureFirst = !_isObscureFirst;
                        });
                      }),
                ),
              ),
              SizedBox(height: 30),
              // buildConformPassFormField(),
              FormBuilderTextField(
                name: "ConfirmPassword",
                obscureText: _isObscureSecond,
                // obscureText: true,
                onSaved: (newValue) => conform_password = newValue!,
                onChanged: (value) {
                  if (value!.isNotEmpty) {
                    removeError(error: kPassNullError);
                  } else if (value.isNotEmpty && password == conform_password) {
                    removeError(error: kMatchPassError);
                  }
                  conform_password = value;
                },
                validator: (value) {
                  if (value!.isEmpty) {
                    addError(error: kPassNullError);
                    return "";
                  } else if ((password != value)) {
                    addError(error: kMatchPassError);
                    return "";
                  }
                  return null;
                },
                decoration: InputDecoration(
                  labelText: "Confirm Password",
                  hintText: "Re-enter your password",
                  // If  you are using latest version of flutter then lable text and hint text shown like this
                  // if you r using flutter less then 1.20.* then maybe this is not working properly
                  floatingLabelBehavior: FloatingLabelBehavior.auto,
                  suffixIcon: IconButton(
                      icon: Icon(
                          _isObscureSecond
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: _isObscureSecond
                              ? Colors.grey
                              : Theme.of(context).primaryColor),
                      onPressed: () {
                        setState(() {
                          _isObscureSecond = !_isObscureSecond;
                        });
                      }),
                ),
              ),
              FormError(errors: errors),
              SizedBox(height: 40),
              DefaultButton(
                isLoading: false,
                text: "Register",
                press: () async {
                  // showLoaderDialog(context);
                  if (_formKey.currentState!.validate()) {
                    _formKey.currentState!.save();

                    var data =
                        Map<String, dynamic>.from(_formKey.currentState!.value);

                    _formKey.currentState!.save();

                    GestureDetector(
                        onTap: () => {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Home()))
                              // Dashboard
                            });
                  }
                },
              ),
            ])),
      ),
    );
  }
}
