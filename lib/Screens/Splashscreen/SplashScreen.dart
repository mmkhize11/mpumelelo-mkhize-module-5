import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:module3/Screens/Login/Login.dart';

import 'package:page_transition/page_transition.dart';
// import 'package:mpumelelo_mkhize_module_4/util/PageTransitionType%20.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        duration: 3000,
        splash: 'assets/logo.jpg',
        nextScreen: Login(),
        splashTransition: SplashTransition.slideTransition,
        pageTransitionType: PageTransitionType.rightToLeft,
        backgroundColor: Colors.white);
  }
}
