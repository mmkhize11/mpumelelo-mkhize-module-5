import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:module3/Widgets/ListItems.dart';
import 'package:module3/Widgets/ProductItem.dart';

class ShirtsPage extends StatefulWidget {
  const ShirtsPage({Key? key}) : super(key: key);

  @override
  State<ShirtsPage> createState() => _ShirtsPageState();
}

class _ShirtsPageState extends State<ShirtsPage> {
  List<ProductItem> ShirtProducts = [];
  final _fireStore = FirebaseFirestore.instance;
  // final _fireAuth = FirebaseAuth.instance;

  //   ProductItem(
  //       name: "NikeCourt Dri-FIT ADV Rafa",
  //       imageUrl:
  //           "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/edc92168-6df2-4e9e-97ed-198c44a96ff7/nikecourt-dri-fit-adv-rafa-short-sleeve-tennis-top-vdFkCF.png",
  //       quantityAvailable: 5,
  //       price: 1400),
  //   ProductItem(
  //       name: "Nike Sportswear",
  //       imageUrl:
  //           "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/10112e62-0ff0-49b6-a035-8d2c3f2a2745/sportswear-overshirt-Q9HDcq.png",
  //       quantityAvailable: 5,
  //       price: 1400),
  //   ProductItem(
  //       name: "Nike Dri-FIT",
  //       imageUrl:
  //           "https://static.nike.com/a/images/t_PDP_864_v1/f_auto,b_rgb:f5f5f5/cf637666-2d0f-4c99-8f9d-54bdf8480082/dri-fit-seamless-training-top-gHjZNW.png",
  //       quantityAvailable: 5,
  //       price: 950),
  // ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListItems(passedCategory: "Shirts"
          // products: ShirtProducts,
          ),
    );
  }
}
