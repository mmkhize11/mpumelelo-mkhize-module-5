import 'package:flutter/material.dart';
import 'package:module3/Screens//Login/LoginBody.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: LoginBody());
  }
}
