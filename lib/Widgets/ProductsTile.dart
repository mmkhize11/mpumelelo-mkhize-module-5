import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:module3/Screens/ProductDetails/ProductDetails.dart';
import 'package:module3/Widgets/ProductItem.dart';
import 'package:module3/Widgets/createProductDialog.dart';

class ProductTile extends StatelessWidget {
  // String name;
  // String imageUrl;
  // String slug;
  // String price;
  // bool fromSubProducts = false;

  ProductItem? passedItem;
  String? firebaseId;
  ProductTile({Key? key, this.passedItem, this.firebaseId
      // @required this.name,
      // @required this.imageUrl,
      // @required this.slug,
      // @required this.price,
      // this.fromSubProducts
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        /* if (fromSubProducts) {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ProductsScreen(
                      slug: "products/?page=1&limit=12&category=" + slug,
                      name: name,
                    )),
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SubCategoryScreen(
                      slug: slug,
                    )),
          );
        }*/
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ProductDetails(product: passedItem)),
        );
      },
      child: Container(
        height: 500,
        padding: EdgeInsets.only(top: 5),
        child: Card(
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.all(
                Radius.circular(8.0),
              ),
            ),
            elevation: 0,
            child: Center(
              child: Column(
                children: <Widget>[
                  Image.network(
                    passedItem!.imageUrl,
                    width: 150,
                    height: 150,
                  ),
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 10, right: 10, top: 15),
                    child: Text(passedItem!.name,
                        // (name.length <= 40 ? name : name.substring(0, 40)),
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Color(0xFF444444),
                            // fontFamily: 'Roboto-Light.ttf',
                            // fontSize: 15,
                            fontWeight: FontWeight.w400)),
                  ),
                  Container(
                    alignment: Alignment.bottomLeft,
                    padding: EdgeInsets.only(left: 10, right: 10, top: 10),
                    child: Text(
                        "R ${(passedItem!.price != null) ? passedItem!.price : 'Unavailable'}",
                        style: TextStyle(
                            color: (passedItem!.price != null)
                                ? Theme.of(context).primaryColor
                                : Color(0xFF0dc2cd),
                            // fontFamily: 'Roboto-Light.ttf',
                            fontSize: 20,
                            fontWeight: FontWeight.w500)),
                  ),
                  Row(
                    children: [
                      SizedBox(width: 10),
                      Ink(
                          decoration: const ShapeDecoration(
                            color: Color(0xFF0dc2cd),
                            shape: CircleBorder(),
                          ),
                          child: IconButton(
                            icon: Icon(Icons.edit),
                            // iconSize: 24,
                            color: Colors.white,
                            onPressed: () async {
                              return await showDialog(
                                  context: context,
                                  builder: (context) {
                                    bool isChecked = false;
                                    return CreateProductDialog(
                                        existingProduct: passedItem,
                                        FirebaseID: firebaseId);
                                  });
                            },
                          )),
                      SizedBox(width: 10),
                      Ink(
                          decoration: const ShapeDecoration(
                            color: Colors.red,
                            shape: CircleBorder(),
                          ),
                          child: IconButton(
                            icon: Icon(Icons.delete),
                            // iconSize: 24,
                            color: Colors.white,
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext ctx) {
                                    return AlertDialog(
                                      title: const Text('Please Confirm'),
                                      content: const Text(
                                          'Are you sure to remove this product'),
                                      actions: [
                                        // The "Yes" button
                                        TextButton(
                                            onPressed: () {
                                              // Remove the box
                                              // Close the dialog
                                              // Navigator.of(context).pop();
                                              DocumentReference
                                                  documentReference =
                                                  FirebaseFirestore.instance
                                                      .collection('Products')
                                                      .doc(firebaseId);

                                              FirebaseFirestore.instance
                                                  .runTransaction((Transaction
                                                      transaction) async {
                                                DocumentSnapshot snapshot =
                                                    await transaction
                                                        .get(documentReference);

                                                if (!snapshot.exists) {
                                                  throw Exception(
                                                      "product does not exist!");
                                                }

                                                transaction
                                                    .delete(documentReference);
                                              }).then((value) => {
                                                        // setState(() {
                                                        // _saving = false;
                                                        Navigator.of(context)
                                                            .pop()
                                                        // })
                                                      });
                                              ;
                                            },
                                            child: const Text('Yes')),
                                        TextButton(
                                            // style: ButtonStyle(
                                            //     foregroundColor:
                                            //         Theme.of(context)
                                            //             .primaryColor),
                                            onPressed: () {
                                              // Close the dialog
                                              Navigator.of(context).pop();
                                            },
                                            child: const Text('No'))
                                      ],
                                    );
                                  });
                            },
                          ))
                    ],
                  )
                ],
              ),
            )),
      ),
    );
  }
}
