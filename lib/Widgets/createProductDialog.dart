import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:module3/Common/Common.dart';
import 'package:module3/Widgets/ProductItem.dart';
import 'package:module3/Widgets/form_error.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class CreateProductDialog extends StatefulWidget {
  ProductItem? existingProduct;
  String? FirebaseID;
  CreateProductDialog({Key? key, this.existingProduct, this.FirebaseID})
      : super(key: key);

  @override
  State<CreateProductDialog> createState() => _CreateProductDialogState();
}

class _CreateProductDialogState extends State<CreateProductDialog> {
  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  final _formKey = GlobalKey<FormBuilderState>();
  final TextEditingController _textEditingController = TextEditingController();
  bool isChecked = false;
  final _fireStore = FirebaseFirestore.instance;
  late String name, imageUrl;
  String? category = null;
  late int quantityAvailable, price;

  List<String> categories = ["Shoes", "Shirts", "Bottoms"];
  bool _saving = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.existingProduct != null) {
      setState(() {
        category = widget.existingProduct!.category;
      });
    } else {
      category = "Shoes";
    }
  }

  @override
  Widget build(BuildContext context) {
    // return await showDialog(
    // context: context,
    // builder: (context) {
    //   bool isChecked = false;
    return ModalProgressHUD(
      child: StatefulBuilder(builder: (context, setState) {
        return AlertDialog(
          content: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: FormBuilder(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    buildImageUrl(),
                    SizedBox(height: 20),
                    buildProductNameField(),
                    SizedBox(height: 20),
                    buildCategory(),
                    SizedBox(
                      height: 20,
                    ),
                    buildPrice(),
                    SizedBox(height: 20),
                    buildQuantityAvailable(),
                    SizedBox(height: 20),
                    FormError(errors: errors),
                    SizedBox(height: 30),
                  ],
                )),
          ),
          title: widget.existingProduct != null
              ? Text('Edit Product')
              : Text('Create Product'),
          actions: <Widget>[
            InkWell(
              child: Text('Cancel   '),
              onTap: () {
                Navigator.of(context).pop();
                // if (_formKey.currentState!.validate()) {
                //   // Do something like updating SharedPreferences or User Settings etc.
                //   Navigator.of(context).pop();
                // }
              },
            ),
            Visibility(
              visible: widget.existingProduct != null ? true : false,
              child: InkWell(
                child: Text('Update'),
                onTap: () {
                  if (_formKey.currentState!.validate()) {
                    // Do something like updating SharedPreferences or User Settings etc.
                    // _fireStore.
                    setState(() {
                      _saving = true;
                    });

                    _formKey.currentState!.save();
                    print(
                        'values are   ${jsonEncode(_formKey.currentState!.value)}');

                    //Simulate a service call
                    print('submitting to backend...');
                    DocumentReference documentReference = FirebaseFirestore
                        .instance
                        .collection('Products')
                        .doc(widget.FirebaseID);

                    FirebaseFirestore.instance
                        .runTransaction((Transaction transaction) async {
                      DocumentSnapshot snapshot =
                          await transaction.get(documentReference);

                      if (!snapshot.exists) {
                        throw Exception("product does not exist!");
                      }

                      transaction.update(documentReference, {
                        "name": "$name",
                        "category": "$category",
                        "price": price,
                        "imageUrl": "$imageUrl",
                        "quantityAvailable": quantityAvailable
                      });

                      // transaction.
                      //  late String name, category, price, imageUrl;
                      // late int quantityAvailable;
                      // await reference.add({
                      //   "name": "$name",
                      //   "category": "$category",
                      //   "price": price,
                      //   "imageUrl": "$imageUrl",
                      //   "quantityAvailable": quantityAvailable
                      // });
                    }).then((value) => {
                              setState(() {
                                _saving = false;
                                Navigator.of(context).pop();
                              })
                            });

                    // new Future.delayed(new Duration(seconds: 4), () {
                    // setState(() {
                    //   _saving = false;
                    //   Navigator.of(context).pop();
                    // });
                    // });
                  }
                },
              ),
            ),
            Visibility(
              visible: widget.existingProduct != null ? false : true,
              child: InkWell(
                child: Text('Create'),
                onTap: () {
                  if (_formKey.currentState!.validate()) {
                    // Do something like updating SharedPreferences or User Settings etc.
                    // _fireStore.
                    setState(() {
                      _saving = true;
                    });

                    _formKey.currentState!.save();
                    print(
                        'values are   ${jsonEncode(_formKey.currentState!.value)}');

                    //Simulate a service call
                    print('submitting to backend...');
                    FirebaseFirestore.instance
                        .runTransaction((Transaction transaction) async {
                      CollectionReference reference =
                          FirebaseFirestore.instance.collection('Products');
                      //  late String name, category, price, imageUrl;
                      // late int quantityAvailable;
                      await reference.add({
                        "name": "$name",
                        "category": "$category",
                        "price": price,
                        "imageUrl": "$imageUrl",
                        "quantityAvailable": quantityAvailable
                      });
                    }).then((value) => {
                              setState(() {
                                _saving = false;
                                Navigator.of(context).pop();
                              })
                            });

                    // new Future.delayed(new Duration(seconds: 4), () {
                    // setState(() {
                    //   _saving = false;
                    //   Navigator.of(context).pop();
                    // });
                    // });
                  }
                },
              ),
            ),
          ],
        );
      }),
      inAsyncCall: _saving,
    );
    // });
  }

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error!);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  FormBuilderTextField buildImageUrl() {
    return FormBuilderTextField(
      name: "imageUrl",
      initialValue: widget.existingProduct != null
          ? widget.existingProduct!.imageUrl
          : "",
      enableInteractiveSelection: true,
      keyboardType: TextInputType.text,
      onSaved: (newValue) => imageUrl = newValue!,
      onChanged: (value) {
        if (value!.isNotEmpty) {
          removeError(error: kImagueUrlNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kImagueUrlNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Image",
        hintText: "Enter Image Url",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        // suffixIcon: Icon(Icons.currency_pound, color: Colors.blue),
      ),
    );
  }

  FormBuilderTextField buildQuantityAvailable() {
    return FormBuilderTextField(
      name: "quantityAvailable",
      initialValue: widget.existingProduct != null
          ? widget.existingProduct!.quantityAvailable.toString()
          : "",
      keyboardType: TextInputType.number,
      onSaved: (newValue) => quantityAvailable = int.parse(newValue!),
      onChanged: (value) {
        if (value!.isNotEmpty) {
          removeError(error: kquantityAvailable);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kquantityAvailable);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Quantity",
        hintText: "Enter Amount in stock",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        // suffixIcon: Icon(Icons.currency_pound, color: Colors.blue),
      ),
    );
  }

  FormBuilderTextField buildPrice() {
    return FormBuilderTextField(
      name: "price",
      initialValue: widget.existingProduct != null
          ? widget.existingProduct!.price.toString()
          : "",
      keyboardType: TextInputType.number,
      onSaved: (newValue) => price = int.parse(newValue!),
      onChanged: (value) {
        if (value!.isNotEmpty) {
          removeError(error: kPrice);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "Price",
        hintText: "Enter your Product Price",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        // suffixIcon: Icon(Icons.currency_pound, color: Colors.blue),
      ),
    );
  }

  FormBuilderTextField buildProductNameField() {
    return FormBuilderTextField(
      name: "name",
      initialValue:
          widget.existingProduct != null ? widget.existingProduct!.name : "",
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue!,
      onChanged: (value) {
        if (value!.isNotEmpty) {
          removeError(error: kNameNullError);
        }
        return null;
      },
      validator: (value) {
        if (value!.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "name",
        hintText: "Enter your Product Name",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
      ),
    );
  }

  FormBuilderDropdown buildCategory() {
    return FormBuilderDropdown<String>(
      // autovalidate: true,
      name: 'category',
      initialValue: category != null ? category : "Shoes",
      // initialValue: widget.existingProduct != null
      //     ? categories.firstWhere(
      //         (element) => element == widget.existingProduct!.category)
      //     : "",
      decoration: InputDecoration(
        labelText: 'Category',
      ),
      // initialValue: 'Male',
      allowClear: true,
      hint: const Text('Select Category'),
      // validator: FormBuilderValidators.compose(
      //     [FormBuilderValidators.required()]),
      items: categories
          .map((gender) => DropdownMenuItem(
                alignment: AlignmentDirectional.center,
                value: gender,
                child: Text(gender),
              ))
          .toList(),
      onChanged: (val) {
        setState(() {
          category = val!;
        });
      },
      valueTransformer: (val) => val?.toString(),
    );
  }
}
