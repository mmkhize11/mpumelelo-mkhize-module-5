// import 'package:notes_of_nature/constants/constants.dart';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// import '../constants/constants.dart';
// import '../size_config.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key? key,
    required this.text,
    required this.press,
    required this.isLoading,
  }) : super(key: key);
  final String text;
  final Function press;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    return Platform.isAndroid
        ? SizedBox(
            width: double.infinity * 0.9,
            height: 48,
            child: ElevatedButton(
              onPressed: () => press(),
              style: ElevatedButton.styleFrom(
                  shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(8.0),

                    // color:
                  ),
                  primary: Theme.of(context).primaryColor),
              child: isLoading == true
                  ? CircularProgressIndicator()
                  : Text(
                      text,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
            ))
        : Container(
            width: MediaQuery.of(context).size.width,
            child: CupertinoButton.filled(
              child: isLoading == true
                  ? CircularProgressIndicator(
                      color: Colors.white,
                    )
                  : Center(child: Text(text)),
              onPressed: () => press(),
              alignment: Alignment.center,
              padding: EdgeInsets.all(8.0),
            ),
          );
    // FlatButton(
    //   shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
    //   color: Colors.blue,
    //   onPressed: press(),
    //   child: Text(
    //     text,
    //     style: TextStyle(
    //       fontSize: getProportionateScreenWidth(18),
    //       color: Colors.white,
    //     ),
    //   ),
    // ),
  }
}
