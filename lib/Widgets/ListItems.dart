import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:module3/Widgets/ProductItem.dart';
import 'package:module3/Widgets/ProductsTile.dart';

class ListItems extends StatefulWidget {
  // List<ProductItem>? products;
  String passedCategory;
  ListItems({Key? key, required this.passedCategory}) : super(key: key);

  @override
  State<ListItems> createState() => _ListItemsState();
}

class _ListItemsState extends State<ListItems> {
  final _fireStore = FirebaseFirestore.instance;
  @override
  Widget build(BuildContext context) {
    return getGroupsWidget();
//     return GridView.count(
//       crossAxisCount: 2,
// //    physics: NeverScrollableScrollPhysics(),
//       padding: EdgeInsets.all(1.0),
//       childAspectRatio: 8.0 / 12.0,
//       children: List<Widget>.generate(widget.products!.length, (index) {
//         return GridTile(
//             child: ProductTile(
//           passedItem: widget.products![index],
//         ));
//       }),
//     );
  }

  Widget getGroupsWidget() {
    return FutureBuilder<QuerySnapshot>(
      builder: (context, querySnapshot) {
        if (!querySnapshot.hasData) {
          print('${querySnapshot.toString()}');
          return Center(child: CircularProgressIndicator());
        }

        return Expanded(
          flex: 2,
          child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 250,
                  mainAxisExtent: 300,
                  // childAspectRatio: 3 / 2,
                  // childAspectRatio: 3 / 2,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 50),
              itemCount: querySnapshot.data!.docs!.length,
              itemBuilder: (BuildContext ctx, index) {
                DocumentSnapshot document = querySnapshot.data!.docs![index];
                final data = document.data()! as Map<String, dynamic>;
                ProductItem currentItem = ProductItem.fromMap(data);
                // ProductItem.fromJson(data).copyWith(name: doc.id)
                return GridTile(
                    child: ProductTile(
                        passedItem: currentItem,
                        firebaseId: querySnapshot.data!.docs![index].id));
              }),
        );
        // return ListView.builder(
        //   itemBuilder: (context, index) {
        //     DocumentSnapshot document = snapshot.data.docs[index];

        //     return ProductTile(
        //       passedItem: document,
        //     );
        //   },
        //   itemCount: snapshot.data.docs.length,
        // );
      },
      future: loadProducts(widget.passedCategory),
    );
  }

  Future<QuerySnapshot> loadProducts(passedCategoryvalue) async {
    return await _fireStore
        .collection("Products")
        .where("category", isEqualTo: passedCategoryvalue)
        .get();
  }
}
